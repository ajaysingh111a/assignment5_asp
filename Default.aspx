﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:CheckBoxList ID="CheckBoxList1" runat="server" Width="251px" RepeatDirection="Horizontal">
            <asp:ListItem Value="1">Asp.net</asp:ListItem>
            <asp:ListItem Value="2">C#</asp:ListItem>
            <asp:ListItem Value="3">VB</asp:ListItem>
            <asp:ListItem Value="4">WCF</asp:ListItem>
            <asp:ListItem Value="5">LINQ</asp:ListItem>
            <asp:ListItem Value="6">MVC</asp:ListItem>
        </asp:CheckBoxList>

    </div>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Get Selected Values" />
&nbsp;&nbsp;&nbsp;
      <input type="reset" value="Clear Selection"><br><br>

        Selected Values are =

        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
    </form>
</body>
</html>
